ruleset io.picolabs.twilio_v2 {
  meta {
    configure using account_sid = ""
                    auth_token = ""
    provides
        send_sms,
        get_messages
  }
 
  global {
    send_sms = defaction(to, from, message) {
       base_url = <<https://#{account_sid}:#{auth_token}@api.twilio.com/2010-04-01/Accounts/#{account_sid}/>>
       http:post(base_url + "Messages.json", form = {
                "From":from,
                "To":to,
                "Body":message
            })
    }

    get_messages = function(to, from, pageSize) {
      base_url = <<https://#{account_sid}:#{auth_token}@api.twilio.com/2010-04-01/Accounts/#{account_sid}/>>;
      
      noFilter = {};
      toFilter = to == "" => noFilter | noFilter.put("To", to);
      fromToFilter = from == "" => toFilter | toFilter.put("From", from);
      allFilters = pageSize == "" => fromToFilter | fromToFilter.put("PageSize", pageSize);
      
      http:get(base_url + "Messages.json", allFilters){"content"}.decode();
    }
  }
}
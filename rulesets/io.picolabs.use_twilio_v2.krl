ruleset io.picolabs.use_twilio_v2 {
  meta {
    shares __testing, getResult
    use module io.picolabs.lesson_keys
    use module io.picolabs.twilio_v2 alias twilio
        with account_sid = keys:twilio{"account_sid"}
             auth_token =  keys:twilio{"auth_token"}
  }
  global {
    getResult = function() {
      ent:result;
    }
    __testing = { "queries": [  { "name": "getResult"}],
                  "events": [] 
    }
  }
  
  rule test_send_sms {
    select when test new_message
    twilio:send_sms(event:attr("to"),
                    event:attr("from"),
                    event:attr("message")
                   )
  }

  rule test_get_messages {
    select when test get_messages
    pre {
        to = event:attr("To") != null => event:attr("To") | "";
        from = event:attr("From") != null => event:attr("From") | "";
        pageSize = event:attr("PageSize") != null => event:attr("PageSize") | "";
        result = twilio:get_messages(to, from, pageSize);
    }
    send_directive("SMS", result)
    fired {
      ent:result := result
    }
  }
}
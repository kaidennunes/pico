ruleset manage_sensors {
  meta {
    shares __testing, sensors, temperatures, temperatureReports
    provides __testing, sensors, temperatures, temperatureReports
    use module sensor_manager_profile alias profile
    use module io.picolabs.wrangler alias wrangler
    use module io.picolabs.subscription alias wrangler_subscription
  }
  global {
    threshold = 80;
    location = "Provo"
    temperatureSensorPicoRole = "TemperatureSensor"
    temperatureSensorManagerPicoRole = "TemperatureSensorManager"
    necessaryRulesets = [
      "temperature_store",
      "wovyn_base",
      "sensor_profile"
    ];
    
    temperatureReports = function () {
      reportKeys = ent:temperatureReports.defaultsTo({}).keys().klog("Report keys: ");
      lastReport = ent:temperatureReportID.defaultsTo(0) - 1;
      startReport = lastReport - 4;
      startReport = (startReport < 0) => 0 | startReport;
      lastReportKeys = reportKeys.splice(0,startReport);
      ent:temperatureReports.defaultsTo({}).filter(function(v,k){
        lastReportKeys.any(function(key){
          key == k
        });
      }).klog();
    }
    
    temperatures = function () {
      getTemperatureSensorSubscriptions().defaultsTo([]).reduce(function(a,b){
          tx_host = b["Tx_host"] != null => Tx_host | meta:host;
          a.append(wrangler:skyQuery(b["Tx"],"temperature_store","temperatures",null, tx_host));
        }, []).klog();
    }
    
    sensors = function () {
      ent:sensors.defaultsTo([]);
    }
    
    getWellKnownRx = function(eci, tx_host) {
      url = tx_host+"/sky/cloud/"+eci+"/io.picolabs.subscription/wellKnown_Rx";
      http:get(url.klog("url")){"content"}.decode(){"id"}
    }

    getTemperatureSensorSubscriptions = function() {
        wrangler_subscription:established().defaultsTo([]).filter(function(subscription) {
          subscription["Tx_role"] == temperatureSensorPicoRole;
        });
    }
    
    __testing = { "queries":
      [ { "name": "__testing" },
      { "name": "sensors" },
       { "name": "temperatures" },
       { "name": "temperatureReports" }
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ 
        { "domain": "sensor", "type": "new_sensor", "attrs": [ "Name" ] },
        { "domain": "sensor", "type": "unneeded_sensor", "attrs": [ "Name" ] },
        { "domain": "sensor", "type": "create_report", "attrs": [ ] },
        { "domain": "sensor", "type": "delete_reports", "attrs": [ ] }
      ]
    }
  }
  
  rule report_received {
    select when sensor report_received
    pre {
      reportExists = ent:temperatureReports.defaultsTo({}){event:attr("correlationID")} != null;
    }
    if reportExists then every {
        send_directive("say", {"Results": "Adding to report, please wait"});
    }
    fired {
        reports = ent:temperatureReports;
        temperatureReports = ent:temperatureReports{event:attr("correlationID")}{"temperatures"}.append(event:attr("temperatures"));
        reportedSensors = ent:temperatureReports{event:attr("correlationID")}{"responding"} + 1;
        temperatureSensor = ent:temperatureReports{event:attr("correlationID")}{"temperature_sensors"};
        ent:temperatureReports{event:attr("correlationID")} := {"temperature_sensors": temperatureSensor, "responding": reportedSensors, "temperatures": temperatureReports};
    }
    else {
      raise sensor event "send_directive_message" attributes {
        "message":"Creating initial report, please wait"
      };
      ent:temperatureReports{event:attr("correlationID")} := {"temperature_sensors": getTemperatureSensorSubscriptions().length(), "responding": 1, "temperatures": event:attr("temperatures")};
    }
  }
  
  rule delete_reports {
    select when sensor delete_reports
    send_directive("say", {"Results": "Deleted all reports and reset the IDs"});
    fired {
        ent:temperatureReports := {};
        ent:temperatureReportID := 0;
    }
  }
  rule create_report {
    select when sensor create_report
    foreach getTemperatureSensorSubscriptions() setting (temperatureSensorSubscription)
    pre {
      tx_host = temperatureSensorSubscription["Tx_host"] != null => Tx_host | meta:host;
    }
    event:send({
      "eci":temperatureSensorSubscription["Tx"], 
      "domain":"wovyn", 
      "type":"create_report", 
      "attrs":{
        "host": meta:host,
        "eci": temperatureSensorSubscription["Rx"],
        "correlationID": ent:temperatureReportID.defaultsTo(0)
      }
    },tx_host);

    fired {
      klog("Requested report");
      ent:temperatureReportID := ent:temperatureReportID.defaultsTo(0) + 1 on final;
    }
  }
  
  rule new_sensor {
    select when sensor new_sensor where event:attr("Name") != null
    pre {
      notDuplicateName = ent:sensors.defaultsTo([]).filter(function(picoSensor) {
          picoSensor["Name"] == event:attr("Name");
      }).length() == 0;
    }
    
    if notDuplicateName then every {
      send_directive("say", {"Results": "Creating child pico, please wait"});
    }
    
    fired {
      // Create new pico and install temperature_store, wovyn_base, and sensor_profile rulesets in new pico
      raise wrangler event "child_creation"
        attributes { 
          "name": event:attr("Name"),
          "rids": necessaryRulesets
      };
    }
    else {
      raise sensor event "send_directive_message" attributes {
        "message":"Duplicate pico name, please choose another name"
      };
    }
  }
    
  rule pico_created {
    select when wrangler child_initialized
    // Set child pico's name, notification SMS number, and default threshold
    pre {
        smsNumbers = profile:smsNumbers().klog();
    }
    event:send({
        "eci":event:attr("eci"), 
        "domain":"sensor", 
        "type":"profile_updated", 
        "attrs":{
          "Name" : event:attr("name"),
          "Location" : location,
          "ThresholdTemperature" : threshold,
          "FromSMSNumber" :  smsNumbers["fromSMSNumber"],
          "ToSMSNumber" : smsNumbers["toSMSNumber"]
        }
      });
    
    fired {
      raise wrangler event "subscription"
        attributes { 
          "name": event:attr("name"),
          "wellKnown_Tx": getWellKnownRx(event:attr("eci"), meta:host), 
          "channel_type":"subscription",
          "Rx_role":temperatureSensorManagerPicoRole,
          "Tx_role":temperatureSensorPicoRole
      };
    }
  }
  
  rule subscription_added {
    select when wrangler subscription_added where temperatureSensorPicoRole == event:attr("Rx_role").klog("Rx role: ")  && temperatureSensorManagerPicoRole == event:attr("Tx_role")
    fired {
      // Map name and Tx to entity variable
      ent:sensors := ent:sensors.defaultsTo([]).append(
      {
        "Name": event:attr("name"),
        "Tx":  event:attr("Tx")
      });
    }
  }
  
  rule introduce_sensor_pico {
    select when sensor introduce_sensor
    pre {
      tx_host = event:attr("tx_host") != null => event:attr("Tx_host") | meta:host;
      eci = event:attr("eci");
      name = event:attr("name") != null => event:attr("name") | "Introduced Pico " + ent:sensors.defaultsTo([]).length();
    }
    if eci != null then every {
        send_directive("say", {"Results": "Creating subscription, please wait"});
    }
    fired {
        raise wrangler event "subscription"
        attributes {
          "name": name,
          "wellKnown_Tx": getWellKnownRx(eci, tx_host), 
          "Tx_host": tx_host,
          "channel_type":"subscription",
          "Rx_role":temperatureSensorManagerPicoRole,
          "Tx_role":temperatureSensorPicoRole
      };
    }
    else {
      raise sensor event "send_directive_message" attributes {
        "message":"Target pico's eci not defined, please define eci"
      };
    }
  }
  rule delete_pico {
    select when sensor unneeded_sensor 
    pre {
      pico = ent:sensors.defaultsTo([]).filter(function(picoSensor) {
          picoSensor["Name"] == event:attr("Name");
      }).head();
    }
    
    if pico != null then every {
        send_directive("say", {"Results": "Deleting child pico and/or subscription now, please wait"});
    }
    
    fired {
      // Delete child pico subscription
      raise wrangler event "subscription_cancellation"
        attributes {
          "Tx":pico["Tx"]
        };
      
      ent:sensors := ent:sensors.defaultsTo([]).filter(function(picoSensor) {
          picoSensor["Name"] != event:attr("Name");
      });
      raise wrangler event "child_deletion"
        attributes {
          "name": event:attr("Name")
        };
    } else {
      raise sensor event "send_directive_message" attributes {
        "message":"Please send the name of a currently activated child pico to delete"
      };
    }
    
  }
  
  rule send_directive_message {
    select when sensor send_directive_message
      send_directive("say", {"Results": event:attr("message")});
  }
}
ruleset sensor_manager_profile {
  meta {
    shares __testing, smsNumbers
    provides __testing, smsNumbers
    use module io.picolabs.lesson_keys
    use module io.picolabs.twilio_v2 alias twilio
          with account_sid = keys:twilio{"account_sid"}
          auth_token =  keys:twilio{"auth_token"}
  }
  global {
    fromSMSNumber = "+13853753036";
    toSMSNumber = "+18014270658";
    __testing = { "queries":
      [ { "name": "__testing" }
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ //{ "domain": "d1", "type": "t1" }
      //, { "domain": "d2", "type": "t2", "attrs": [ "a1", "a2" ] }
      ]
    }
    
    smsNumbers = function () {
      {
        "fromSMSNumber": fromSMSNumber,
        "toSMSNumber": toSMSNumber
      }
    }
  }
  
  rule threshold_notification {
    select when sensor threshold_violation
    twilio:send_sms(toSMSNumber,
                    fromSMSNumber,
                    "Temperature " + event:attr("temperature") +" has exceeded threshold."
                   );
    fired {
      klog("Sent SMS");
    }
  }
}
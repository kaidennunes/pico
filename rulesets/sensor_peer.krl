ruleset sensor_peer {
  meta {
    shares __testing, getSeenMessages, getSeenMessagesSummary, getPeerSeenMessagesSummary
    use module temperature_store alias store
    use module io.picolabs.subscription alias wrangler_subscription
  }
  global {
    __testing = { "queries":
      [ 
        { "name": "__testing" },
        { "name": "getSeenMessages" },
        { "name": "getSeenMessagesSummary" },
        { "name": "getPeerSeenMessagesSummary" }
      ] , "events":
      [ 
        { "domain": "gossip", "type": "start_heartbeat" },
        { "domain": "gossip", "type": "clear_messages" },
        { "domain": "gossip", "type": "process", "attrs":["status"] }
      //, { "domain": "d2", "type": "t2", "attrs": [ "a1", "a2" ] }
      ]
    }
    peerRole = "peer"

    getSeenMessages = function () {
      ent:seenMessages.defaultsTo({});
    }
    
    getSeenMessagesSummary = function () {
      ent:seenMessagesSummary.defaultsTo({});
    }
    
    getPeerSeenMessagesSummary = function () {
      ent:peerSeenMessagesSummary.defaultsTo({});
    }
    
    getPeers = function() {
        wrangler_subscription:established().defaultsTo([]).filter(function(subscription) {
          subscription["Tx_role"] == peerRole;
        });
    }
    
    getPeer = function() {
      // Get all peers
      peers = getPeers();
      peers.length().klog("Number of peers");
      neededPeers = peers.filter(function(peer){
        mySeenMessages = getSeenMessages();
        peerNeededMessages = mySeenMessages.filter(function(message,messageID){
          messageIDTokens = messageID.split(re#:#);
          originID = messageIDTokens[0];
          messageNumber = messageIDTokens[1].as("Number");
          peerSeenMessages = getPeerSeenMessagesSummary(){peer["Tx"]}.defaultsTo({});
          peerSeenMessages{originID} == null || peerSeenMessages{originID} < messageNumber;
        });
        peerNeededMessages.keys().length() > 0;
      });
  
      neededPeers.length().klog("Needed Peers Count");

      // Randomly choose one of those that need something.
      peerID = neededPeers.length() > 0 => random:integer(0, neededPeers.length() - 1) | -1;
      
      // If none need something, then just choose a random peer
      chosenPeer = (peerID < 0) => peers[random:integer(peers.length() - 1)] | neededPeers[peerID];
      
      chosenPeer.klog("Chosen Peer ID");

      chosenPeer;
    }
    prepareMessage = function(subscriber) {
      peerNeededMessages = getSeenMessages().filter(function(message,messageID){
          messageIDTokens = messageID.split(re#:#);
          originID = messageIDTokens[0];
          messageNumber = messageIDTokens[1].as("Number");
          peerSeenMessages = getPeerSeenMessagesSummary(){subscriber["Tx"]}.defaultsTo({});
          peerSeenMessages{originID} == null || peerSeenMessages{originID} < messageNumber;
        });
        peerNeededMessages.keys().length() > 0;
        
      // Get index of message to send (choose first one)
      neededMessageFromOriginIndex = peerNeededMessages != null && peerNeededMessages.keys().length() > 0 => 0 | -1;
     // neededMessageFromOriginIndex = peerNeededMessages != null && peerNeededMessages.keys().length() > 0 => random:integer(peerNeededMessages.length() - 1) | -1;
      

      preparedMessage = neededMessageFromOriginIndex >= 0 => heardRumor(peerNeededMessages, neededMessageFromOriginIndex) | temperatureRumor();
     
      preparedMessage.klog("Prepared message: ");
      
      preparedMessage = preparedMessage == null => seen(subscriber) | preparedMessage;
            
      preparedMessage.klog("Final Prepared message: ");
      
      preparedMessage
    }
    
    temperatureRumor = function() {
      // Find if there is a temperature rumor that this pico needs to propagate
      temperatureReadings = store:temperatures();
      myTemperatureReadings = temperatureReadings != null => temperatureReadings{meta:picoId} | null;
      
      indexOfReadingToBeSent = myTemperatureReadings != null => ent:messageNumber.defaultsTo(0) | -1;

      readingToBeSent = indexOfReadingToBeSent < 0  => null |  myTemperatureReadings{indexOfReadingToBeSent};
      
      
      
      rumor = readingToBeSent == null => null | {
        "MessageID": meta:picoId + ":" + ent:messageNumber.defaultsTo(0),
        "SensorID": meta:picoId,
        "Temperature": readingToBeSent{"temperature"},
        "Timestamp": readingToBeSent{"timestamp"}
      };
      
      preparedMessage = rumor == null => null | {"message":rumor, "type": "rumor", "update": true};
      preparedMessage;
    }
    
    seen = function(subscriber) {
      {"message":getSeenMessagesSummary(), "type": "seen" };
    }
    
    heardRumor = function(peerNeededMessages, messageID) {
      // Get the key (origin ID) and the latest message they saw
      messageOriginID = peerNeededMessages.keys()[messageID];
      highestSeenMessage = peerNeededMessages{messageOriginID};

      // Get the message they need
      seenMessage = getSeenMessages(){messageOriginID};
      
      // Mark that this rumor came from this pico
      message = seenMessage.put(["SensorID"], meta:picoId);
      {"message":message, "type": "rumor" };
    }
  }

  rule gossip_heartbeat_restart {
    select when gossip gossip_heartbeat
    fired {
      // Restart heatbeat
      schedule gossip event "gossip_heartbeat" repeat time:add(time:now(), {"seconds": 5}) attributes event:attrs
    }
  }
  rule gossip_heartbeat {
    select when gossip gossip_heartbeat where ent:messageStatus && getPeers().length() > 0
    pre {
      subscriber = getPeer();          
      message = prepareMessage(subscriber);   
      tx_host = subscriber["Tx_host"] != null => subscriber["Tx_host"] | meta:host;
    }
    
    if ent:messageStatus.defaultsTo(true) && (message != null && ((message["message"].keys().length() > 0) || message["type"] == "seen")) then every {
        send_directive("say", {"Results": "Sending message"});
    }
    
    fired {
      subscriber.klog("Subscriber: ");          
      message.klog("Message: ");
      tx_host.klog("Host: ");

      // Send messages and update state
      raise gossip event "send_message" attributes {
        "subscriber":subscriber,
        "message":message,
        "tx_host":tx_host
      };
    
      updatedMessageNumber = message{"update"} != null => ent:messageNumber.defaultsTo(0) + 1 | ent:messageNumber.defaultsTo(0);
      ent:messageNumber := updatedMessageNumber.klog("Updated message number: ");
      
      // Update the seen messages entity variable, so this sensor can still inform other sensors
      ent:seenMessages := message{"update"} != null => getSeenMessages().put([message["message"]{"MessageID"}], message["message"]) | getSeenMessages();
      
      // Mark that the peer has seen this message
      messageID = message["message"]{"MessageID"};
      messageIDTokens = messageID == null => null | messageID.split(re#:#);
      originID = messageID == null => null | messageIDTokens[0];
      messageNumber = messageID == null => null | messageIDTokens[1].as("Number");
      
      peerSeenMessagesSummary = getPeerSeenMessagesSummary();
      peerSummary = peerSeenMessagesSummary{subscriber["Tx"]}.defaultsTo({});
      updatedPeerSummary = peerSummary.put([originID], messageNumber);
      
      ent:seenMessagesSummary := messageID == null => getSeenMessagesSummary() | getSeenMessagesSummary().put([originID], messageNumber);
      ent:peerSeenMessagesSummary := messageID == null => getPeerSeenMessagesSummary() | peerSeenMessagesSummary.put([subscriber["Tx"]], updatedPeerSummary);
    }
    else {
      message.klog("The message is null . . .");
    }
  }
  
  rule send_message {
    select when gossip send_message
    pre {
      subscriber = event:attr("subscriber");          
      message =  event:attr("message");   
      tx_host =  event:attr("tx_host");
    }

    event:send({
        "eci":subscriber["Tx"], 
        "domain":"gossip", 
        "type":message["type"], 
        "attrs":{
          "message": message["message"],
          "eci": meta:picoId,
          "tx":subscriber["Rx"]
        }
     },tx_host);
     
     fired {
      message.klog("Sent message: ");
    }
  }
  
  rule message_rumor {
    select when gossip rumor where ent:messageStatus.defaultsTo(true)
    pre {
      rumor = event:attr("message");
      messageID = rumor{"MessageID"};
      messageIDTokens = messageID.split(re#:#);
      originID = messageIDTokens[0];
      messageNumberString = messageIDTokens[1];
      messageNumber = messageNumberString.as("Number");
      sensorID = rumor{"SensorID"};
      temperature = rumor{"Temperature"};
      timestamp = rumor{"Timestamp"};
      isNewMessage = getSeenMessages(){messageID} == null;
    }
    
    if isNewMessage then every {
        send_directive("say", {"Results": "Adding message to list"});
    }
    
    fired {
      // Update the seen messages entity variable
      ent:seenMessages := getSeenMessages().put([messageID], rumor);
       
      // Update temperature store
      raise wovyn event "new_temperature_reading" attributes {
        "temperature":temperature,
        "timestamp":timestamp,
        "originID":originID
      };
    }
    else {
      rumor.klog("Message already seen: ");
    }
    finally {
      // Find the seen messages summary from the gossiper
      currentSeenMessageSummary = getSeenMessagesSummary(){originID};
      
      currentSeenMessageSummary.klog("Current seen message summary");
      
      // Either this message is the first one, or there is already a highest seen message
      highestSeenMessageSummary = currentSeenMessageSummary == null => 0 | currentSeenMessageSummary;
      
      highestSeenMessageSummary.klog("Highest seen message summary");

      // Update the highest seen index only if there are no gaps and if this isn't the first entry
      newHighestSeenMessageSummary = (highestSeenMessageSummary + 1 == messageNumber) && currentSeenMessageSummary != null => messageNumber | highestSeenMessageSummary;
      
      newHighestSeenMessageSummary.klog("New highest seen message summary");
       
      // Update the seen messages summary entity variable
      ent:seenMessagesSummary := getSeenMessagesSummary().put([originID], newHighestSeenMessageSummary);
    }
  }
  
  rule message_seen {
      select when gossip seen where ent:messageStatus.defaultsTo(true)
      pre {
          mySeenMessages = getSeenMessages();
          peerSeenMessages = event:attr("message");
          
          neededMessages = mySeenMessages.filter(function(value,key){
            originID = key.split(re#:#)[0];
            peerSeenMessages{originID} == null && originID != event:attr("eci");
          });
          
      }
      
      if neededMessages.keys().length() > 0 then every {
          send_directive("say", {"Results": "Sending messages unseen by peer"});
      }
      
      fired {
        neededMessages.klog("Needed messages: ");
        peerSeenMessages.klog("Peer seen messages: ");
        
        host = event:attr("host") == null => meta:host | event:attr("host");
        raise gossip event "iterate_seen_messages" attributes {
          "neededMessages":neededMessages,
          "peerSeenMessages":peerSeenMessages,
          "eci":event:attr("eci"),
          "tx":event:attr("tx"),
          "host": host
        };
      }
      finally {
          ent:peerSeenMessagesSummary := getPeerSeenMessagesSummary().put([event:attr("tx")], peerSeenMessages);
          ent:peerSeenMessagesSummary.klog("Updated peer summary");
      }
  }
  
  rule iterate_seen_messages {
    select when gossip iterate_seen_messages
    foreach event:attr("neededMessages") setting (message,messageID)
    pre {
      modifiedRumor = message.put(["SensorID"], meta:picoId);
    }
    
    // Send the rumor to the pico raised the seen event. 
    event:send({
      "eci": event:attr("tx"), 
      "domain":"gossip", 
      "type":"rumor", 
      "attrs":{
        "message": modifiedRumor
      }
    },event:attr("host"));
    
    fired {
      event:attr("eci").klog("ECI of asking pico: ");
      event:attr("tx").klog("Tx of asking pico: ");
      modifiedRumor.klog("Rumor sent:");
    }
  }
  
  rule start_heartbeat {
    select when gossip start_heartbeat
    fired {
      klog("Starting heartbeat");
      schedule gossip event "gossip_heartbeat" repeat time:add(time:now(), {"seconds": 5}) attributes event:attrs
    }
  }
  
  rule clear_messages {
    select when gossip clear_messages
    fired {
      ent:messageNumber := 0;
      ent:seenMessages := {};
      ent:seenMessagesSummary := {};
      ent:peerSeenMessagesSummary := {};
      ent:messageStatus := true;
    }
  }
  
  rule change_messaging_status {
    select when gossip process 
    fired {
      newStatus = event:attr("status") == "on" => true | ent:messageStatus.defaultsTo(true);
      newStatus = event:attr("status") == "off" => false | newStatus;
      klog("Messaging Enabled:  " + newStatus);
      ent:messageStatus := newStatus;
    }
  }
}

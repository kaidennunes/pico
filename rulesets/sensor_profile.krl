ruleset sensor_profile {
  meta {
    provides profile
    shares __testing, profile
    use module io.picolabs.subscription alias wrangler_subscription
  }
  global {
    fromSMSNumber = "+13853753036"
    profile = function() {
        ent:profile.defaultsTo({
          "Name" : "Wovyn Sensor",
          "Location" : "Orem",
          "ThresholdTemperature" : 80,
          "FromSMSNumber" : fromSMSNumber,
          "ToSMSNumber" : "+18014270658"
        })
    }
    
    __testing = { "queries":
      [ { "name": "__testing" },
      { "name": "profile" }
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ //{ "domain": "d1", "type": "t1" }
      //, { "domain": "d2", "type": "t2", "attrs": [ "a1", "a2" ] }
      ]
    }
  }
  
  rule autoAccept {
    select when wrangler inbound_pending_subscription_added
    pre {
      attributes = event:attrs;
    }
    always{
      wrangler_subscription:inbound().klog("Inbound subscription");
      attributes.klog();
      raise wrangler event "pending_subscription_approval"
          attributes attributes;       
    }
}

  rule update_profile {
    select when sensor profile_updated
    fired {
      ent:profile := {
          "Name" : event:attr("Name"),
          "Location" : event:attr("Location"),
          "ThresholdTemperature" : event:attr("ThresholdTemperature"),
          "FromSMSNumber" : fromSMSNumber,
          "ToSMSNumber" : event:attr("ToSMSNumber")
        };
    }
  }
}

ruleset wovyn_base {
  meta {
    name "Wovyn base"
    description <<
A wovyn sensor pico
>>
    author "Kaiden Nunes"
    logging on
    shares __testing
    use module sensor_profile alias profile
    use module io.picolabs.subscription alias wrangler_subscription
    use module io.picolabs.lesson_keys
    use module io.picolabs.twilio_v2 alias twilio
          with account_sid = keys:twilio{"account_sid"}
            auth_token =  keys:twilio{"auth_token"}
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ //{ "domain": "d1", "type": "t1" }
      //, { "domain": "d2", "type": "t2", "attrs": [ "a1", "a2" ] }
      ]
    }
     temperatureSensorManagerPicoRole = "TemperatureSensorManager"

    getTemperatureSensorManagerSubscriptions = function() {
        wrangler_subscription:established().defaultsTo([]).filter(function(subscription) {
          subscription["Tx_role"] == temperatureSensorManagerPicoRole;
        });
    }
  }
  
  rule process_heartbeat  {
    select when wovyn heartbeat where event:attr("genericThing") != null
    send_directive("say", {"Results": "Thanks for the reading"});
    fired  {
      raise wovyn event "new_temperature_reading"
          attributes {
            "temperature":event:attr("genericThing")["data"]["temperature"],
            "timestamp":time:now()
          };
    }
  }
  
  rule find_high_temps {
    select when wovyn new_temperature_reading
    pre {
      exceedsThreshold = event:attr("temperature")[0]["temperatureF"] > profile:profile()["ThresholdTemperature"];
    }
    
    if exceedsThreshold then every {
      send_directive("say", {"Results": "There was a temperature violation"});
    }
    
    fired {
      raise wovyn event "threshold_violation" attributes {
        "temperature":event:attr("temperature")[0]["temperatureF"],
        "timestamp":event:attr("timestamp")
      };
    }
    else {
      raise wovyn event "send_directive_message" attributes {
        "message":"There was no temperature violation"
      };
    }
  }
  
  rule send_directive_message {
    select when wovyn send_directive_message
      send_directive("say", {"Results": event:attr("message")});
  }
  
  rule threshold_notification {
    select when wovyn threshold_violation
      foreach getTemperatureSensorManagerSubscriptions() setting(manager)
      http:post(manager["Tx_host"] + "/sky/event/" + manager["Tx"] + "/123/sensor/threshold_violation" , form = {"temperature": event:attr("temperature")})
      fired {
        manager.klog("Manager notified: ");
      }
  }
}

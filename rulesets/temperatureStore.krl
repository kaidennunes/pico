ruleset temperature_store {
  meta {
    provides temperatures, threshold_violations, inrange_temperatures
    shares __testing, temperatures, threshold_violations, inrange_temperatures
  }
  global {
    temperatures = function() {
        ent:temperatureReadings.defaultsTo({})
    }
    
    threshold_violations  = function() {
      ent:thresholdViolations.defaultsTo({})
    }
    
    inrange_temperatures  = function() {
      inRangeTemperatures = ent:temperatureReadings.defaultsTo([])
          .filter(function(reading) {
            not ent:thresholdViolations.defaultsTo([]).any(function(violation){
                reading["timestamp"] == violation["timestamp"];
              }) || ent:thresholdViolations.defaultsTo([]).length() == 0;
          });
      inRangeTemperatures
    }
    
    __testing = { "queries":
      [ { "name": "__testing" },
      { "name": "temperatures" },
            { "name": "threshold_violations" },
                  { "name": "inrange_temperatures" }

//, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ { "domain": "sensor", "type": "reading_reset" }
      //, { "domain": "d2", "type": "t2", "attrs": [ "a1", "a2" ] }
      ]
    }
  }
  
  rule create_report {
    select when wovyn create_report
    event:send({
      "eci":event:attr("eci"), 
      "domain":"sensor", 
      "type":"report_received", 
      "attrs":{
        "temperatures":temperatures(),
        "correlationID":event:attr("correlationID")
      }
    },event:attr("host"));
    
    fired {
      klog("Report request received: ");
    } 
  }
  
  rule collect_temperatures  {
    select when wovyn new_temperature_reading
    fired {
      readingsFromOrigin = ent:temperatureReadings.defaultsTo({}){event:attr("originID")};
      id = readingsFromOrigin == null => 0 | readingsFromOrigin.keys().length();
      readingsFromOrigin = readingsFromOrigin == null => {} | readingsFromOrigin;

      temperatureObject = {"temperature": event:attr("temperature"), "timestamp": event:attr("timestamp")};
      readingsFromOrigin = readingsFromOrigin.put([id], temperatureObject);

      ent:temperatureReadings := ent:temperatureReadings.defaultsTo({}).put([event:attr("originID")],readingsFromOrigin);
    }
  }
  
  rule collect_threshold_violations   {
    select when wovyn threshold_violation 
    fired {
      readingsFromOrigin = ent:thresholdViolations.defaultsTo({}){event:attr("originID")};
      id = readingsFromOrigin == null => 0 | readingsFromOrigin.keys().length();
      readingsFromOrigin = readingsFromOrigin == null => {} | readingsFromOrigin;

      temperatureObject = {"temperature": event:attr("temperature"), "timestamp": event:attr("timestamp")};
      readingsFromOrigin = readingsFromOrigin.put([id], temperatureObject);

      ent:thresholdViolations := ent:thresholdViolations.defaultsTo({}).put([event:attr("originID")],readingsFromOrigin);
    }
  }
  
  rule clear_temperatures   {
    select when sensor reading_reset
    fired {
      ent:temperatureReadings := {};
      ent:thresholdViolations := {};
    }
  }
}
